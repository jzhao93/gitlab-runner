{{- define "gitlab-runner.runner-env-vars" }}
- name: CI_SERVER_URL
  value: {{ include "gitlab-runner.gitlabUrl" . }}
- name: CLONE_URL
  value: {{ include "gitlab-runner.gitlabUrl" . }}
- name: RUNNER_REQUEST_CONCURRENCY
  value: {{ default 1 .Values.runners.requestConcurrency | quote }}
- name: RUNNER_EXECUTOR
  value: "ssh"
- name: REGISTER_LOCKED
  {{ if or (not (hasKey .Values.runners "locked")) .Values.runners.locked -}}
  value: "true"
  {{- else -}}
  value: "false"
  {{- end }}
- name: RUNNER_OUTPUT_LIMIT
  value: {{ default "" .Values.runners.outputLimit | quote }}
{{- if .Values.runners.cache -}}
{{ include "gitlab-runner.cache" . }}
{{- end }}
{{- if .Values.envVars -}}
{{ range .Values.envVars }}
- name: {{ .name }}
  value: {{ .value | quote }}
{{- end }}
{{- end }}
{{- end }}
